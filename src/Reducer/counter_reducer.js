import { INCREASE_NUMBER, DECREASE_NUMBER } from "../const";
export default function Counter(state = 99, action) {
  let num = 0;

  switch (action.type) {
    case INCREASE_NUMBER:
      console.log("reducers_Increase_number:", state, action);
      num = num = action.num + 1;
      return { num };
    case DECREASE_NUMBER:
      console.log("reducers_Increase_number:", state, action);
      num = num = action.num - 1;
      return { num };
    default:
      num = 0;
      return { num };
  }
}
