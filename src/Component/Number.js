import React, { Component } from "react";
import { connect } from "react-redux";
class Number extends Component {
  render() {
    return (
      <div className="number">
        <h1> Count: </h1>
        <h1> {this.props.num}</h1>
        {this.props.num === 100 ?  <span>Maximum Number Reached </span> : ""}
        {this.props.num === 0 ? <span>Minimum Number Reached</span> : ""}
      </div>
    );
  }
}
function mapStateToProps(state) {
  console.log("mapStatetoProps:--->", state);
  return {
    num: state.num
  };
}

export default connect(mapStateToProps, {})(Number);
