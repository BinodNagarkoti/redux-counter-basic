import React, { Component } from "react";
// import connect from "react-redux";
import "../App.css";

class Button extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <button onClick={this.props.onClick} disabled={this.props.disabled}>
        <span> {this.props.name} </span>
      </button>
    );
  }
}

export default Button;
