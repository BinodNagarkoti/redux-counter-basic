import { INCREASE_NUMBER, DECREASE_NUMBER } from "../const";
export function increaseNumber(num) {
  console.log("increment Action Triger: Provided Value=", num);
  return {
    type: INCREASE_NUMBER,
    num
  };
}

export function decreaseNumber(num) {
  console.log("Decrement Action triger: Provided Value=", num);
  return {
    type: DECREASE_NUMBER,
    num
  };
}
