import React, { Component } from "react";
import "./App.css";
import Header from "./Component/header";
import Number from "./Component/Number";
import { connect } from "react-redux";
import { increaseNumber, decreaseNumber } from "./Action/counter_action";
import { bindActionCreators } from "redux";
import Button from "./Component/Button";
class App extends Component {
  render() {
    console.log("this is from props:", this.props);
    return (
      <main>
        <Header />
        <section>
          <div className="increase">
            <Button
              name="Increase"
              onClick={() => this.props.increaseNumber(this.props.num)}
              disabled={this.props.num === 100 ? true : false}
            />
          </div>
          <Number />

          <div className="decrease">
            <Button
              name="Decrease"
              onClick={() => this.props.decreaseNumber(this.props.num)}
              disabled={this.props.num === 0 ? true : false}
            />
          </div>
        </section>
      </main>
    );
  }
}
function mapStateToProps(state) {
  return {
    num: state.num
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ increaseNumber, decreaseNumber }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
